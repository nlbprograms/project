package nlb.ecpi.ist510;

public class CheckInputs 
{
	private int numberOfObjects;
	private int numberOfTriangle;
	private int numberOfRectangle;
	private int numberOfCircle;
	public CheckInputs(String[] input) 
	{
		StoreInputs(input);
	}
	
	private void StoreInputs(String[] input)
	{
		
		if ((input.length > 0) && (input.length %2 == 0))  
		{
			for (int i=0; i<input.length;)
			{
				if ((input[i].startsWith("c")) || (input[i].startsWith("C")) )  
				{
					this.numberOfCircle = Integer.parseInt(input[i+1]);
				}
				else if ((input[i].startsWith("r")) || (input[i].startsWith("R")) )
				{
					this.numberOfRectangle = Integer.parseInt(input[i+1]);
				}
				else if ((input[i].startsWith("t")) || (input[i].startsWith("T")) )
				{
					this.numberOfTriangle = Integer.parseInt(input[i+1]);
				}
				else
				{	
					System.out.println("there is an error...........");
				}
				i=i+2;
				
			}
		}

		this.numberOfObjects = this.numberOfCircle + this.numberOfTriangle + this.numberOfRectangle;
	}
	
	public int getTotalObject()
	{
		return this.numberOfObjects;
	}
	
	
	
	
	protected void setCircle(int c)
	{
		this.numberOfCircle = c;
	}
	public int getCircle()
	{
		return this.numberOfCircle;
	}
	protected void addCircle(int c)
	{
		this.numberOfCircle = this.numberOfCircle + c;
	}	
	protected void subtractCircle(int c)
	{
		this.numberOfCircle = this.numberOfCircle - c;
	}
	

	
	protected void setRectangle(int r)
	{
		this.numberOfRectangle = r;
	}
	public int getRectangle()
	{
		return this.numberOfRectangle;
	}
	public void addRectangle(int r)
	{
		this.numberOfRectangle = this.numberOfRectangle + r;
	}	
	public void subtractRectangle(int r)
	{
		this.numberOfRectangle = this.numberOfRectangle - r;
	}	
		
	
	protected void setTriangle(int t)
	{
		this.numberOfTriangle = t;
	}
	public int getTriangle()
	{
		return this.numberOfTriangle;
	}
	
	protected void addTriangle(int t)
	{
		this.numberOfTriangle = this.numberOfTriangle + t;
	}	
	protected void subtractTriangle(int t)
	{
		this.numberOfTriangle = this.numberOfTriangle - t;
	}
	

}
