package nlb.ecpi.ist510;

public class Triangle extends ShapeBaseClass implements IshapeClass 
{
	
	protected boolean isRight;
	
	Triangle(int x, int y, int h, int w, int c1, int c2, int c3, int r)
	{
		super(x,y,h,w,c1,c2,c3);

		typeObject = "Triangle";
		setIsRight(r);
	}
	
	
	protected void setIsRight(int r)
	{
		
		if (r == 1 )
		{
			this.isRight = true;
		}
		else
		{
			this.isRight = false;
		}
	}
	
	public boolean getIsRight()
	{
		return this.isRight;
	}
	
	
	public void printObject()
	{
		
	}
	

}
