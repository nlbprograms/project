package nlb.ecpi.ist510;


public class Rectangle extends ShapeBaseClass implements IshapeClass
{


	public Rectangle(int x, int y, int h, int w, int c1, int c2, int c3) 
	{
		super(x,y,h,w,c1,c2,c3);
		super.typeObject = "Rectangle"; 
	}
	

	@Override
	public void printObject() 
	{
		System.out.println("  this item is a Rectangle at x= " + getX() + " y= " + getY() + " " + getColor());
	}

	


	



}
