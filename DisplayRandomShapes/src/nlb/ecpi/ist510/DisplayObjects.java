package nlb.ecpi.ist510;

import acm.graphics.*;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.event.MouseInputListener;

public class DisplayObjects extends JFrame 
{
	private static final long serialVersionUID = 1L;
	
	protected GCanvas canvas;
	protected boolean update;
	protected MouseListener l;
	
	
	DisplayObjects()
	{
		this.setSize(Constrants.CANVUSWIDTH, Constrants.CANVUSHEIGHT);
		canvas = new GCanvas();
		canvas.addMouseListener(l);
		this.add(canvas);
	}
	
	
}
