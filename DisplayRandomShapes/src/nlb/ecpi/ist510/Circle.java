package nlb.ecpi.ist510;

public class Circle extends ShapeBaseClass implements IshapeClass
{
	protected double radius;
	
	Circle(int x, int y, int h, int c1, int c2, int c3)
	{
		super(x, y, h, h, c1, c2, c3);
		setRadius(h);
		super.typeObject = "Circle";
	}
	
	public void setRadius(int h)
	{
		this.radius = h/2; 		
	}
	public double getRadius()
	{
		return this.radius;
	}
	
	@Override
	public void printObject() 
	{
		System.out.println("  this item is a Rectangle at x= " + getX() + " y= " + getY() + " " + getColor());
	}
}
