package nlb.ecpi.ist510;

import java.awt.Color;


abstract class ShapeBaseClass implements IshapeClass
{
	protected String typeObject;
	private int X;
	private int Y;
	private int height;
	private int width;
	private int colorR;
	private int colorG;
	private int colorB;


	
	protected ShapeBaseClass(int x, int y, int h, int w, int c1, int c2, int c3)
	{
		this.X = x;
		this.Y = y;
		this.height = h;
		this.width = w;
		this.colorR = c1;
		this.colorG = c2;
		this.colorB = c3;
	}

	// getters
	public int getX()
	{
		return this.X;
	}
	public int getY()
	{
		return this.Y;
	}
	public Color getColor()
	{
		return new Color(this.colorR, this.colorG, this.colorB);
	}
	public int getHeight()
	{
		return this.height;
	}
	public int getWidth()
	{
		return this.width;
	}
	public String getType()
	{
		return typeObject;
	}

	
	
	// setter
	protected void setX(int x)
	{
		this.X = x;
		
	}
	protected void setY(int y)
	{
		this.Y = y;
	}
	protected void setColor(int c1, int c2, int c3)
	{
		this.colorR = c1;
		this.colorG = c2;
		this.colorB = c3;
	}
	public void setHeight(int h) 
	{
		this.height = h;
	}
	public void setWidth(int w) 
	{
		this.width = w;
	}
	public void setSquare(int h)
	{
		this.width = h;
		this.height = h;
	}
	

	@Override
	public void printObject()
	{
		
		
	}

 

}
	

