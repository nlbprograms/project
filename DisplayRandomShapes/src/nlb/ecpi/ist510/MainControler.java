package nlb.ecpi.ist510;


import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;



import acm.graphics.GOval;
import acm.graphics.GPolygon;
import acm.graphics.GRect;
import acm.program.GraphicsProgram;


public class MainControler extends GraphicsProgram implements MouseListener 
{

	private static final long serialVersionUID = 1L;
	
	boolean update = false;
	protected int TotalObject;
	private CheckInputs input;      //  int x = input.getCircle();  int x = input.getCircle(); //  int x = input.getRectangle();
	private ShapeBaseClass[] shapeList;  //  int x = shapeList.length();
	private ShapeBaseClass[] shapeListTemp;  //  int x = shapeList.length();

	
	protected MainControler() 
	{

	}
	
	
	
	protected void startProgram(String[] in)
	{
		
		this.input = new CheckInputs(in); // check inputs from args 
		this.shapeList = createObjects();    //create Objects
		this.TotalObject = input.getTotalObject();
		
		createRamdomNewShape();
		printObjects();
		this.start();
	}
	
	
	
	protected void createRamdomNewShape()
	{
		ShapeBaseClass[] unknownShape = new ShapeBaseClass[1];
		System.out.println("unknownShape length " + unknownShape.length);
		
		shapeListTemp = (ShapeBaseClass[])shapeList.clone();  // create copy
		System.out.println("shapeListTemp length " + shapeListTemp.length);
		
		
		int x = createRandom(3);   // get random number
		System.out.println("createRandom " + x);
		
		switch (x)    
		{
			case 0:
				{
					unknownShape[0] = createRectangle();
					System.out.println(unknownShape[0].getType());
					break;
				}
			case 1:
			{
				
				unknownShape[0] = createCircle();
				System.out.println(unknownShape[0].getType());
				break;
			}
			case 2:
			{
				unknownShape[0] = createTriangle();
				System.out.println(unknownShape[0].getType());
				break;
			}
			default:
			{
				System.out.println("None");
				break;
			}
		}
		
		
		
		
		this.shapeList = new ShapeBaseClass[shapeListTemp.length+unknownShape.length];  //crate shapeList
		System.out.println("shapeList " + shapeList.length);
		
		
		for (int i=0; i<shapeListTemp.length;i++)
		{
			shapeList[i] = shapeListTemp[i];
		}
		for (int i=shapeListTemp.length; i<shapeListTemp.length+unknownShape.length;i++)
		{
			shapeList[i] = shapeListTemp[0];
		}

		
	}
	
	// mouse area
	public void mousePressed(int buttonEvent, int x, int y)
	{
		
		//System.out.println("mousePressed");
		
		
	}
	@Override
	public void mouseClicked(MouseEvent e) 
	{
		createRamdomNewShape();
		System.out.println("mouseClicked");

	}


	@Override
	public void mouseEntered(MouseEvent e) 
	{
		
		//System.out.println("mouseEntered");
		
		
	}
	@Override
	public void mouseExited(MouseEvent e) 
	{
		
		//System.out.println("mouseExited");
	
		
	}
	@Override
	public void mousePressed(MouseEvent e) 
	{
	
		
		//System.out.println("mousePressed");
		
		
	}
	@Override
	public void mouseReleased(MouseEvent e) 
	{
		
		//System.out.println("mouseReleased");
		
		
	}
	@Override
	public void mouseDragged(MouseEvent arg0) 
	{
		
		//System.out.println("mouseDragged");
		
		
	}
	@Override
	public void mouseMoved(MouseEvent arg0) 
	{
		
		//System.out.println("mouseMoved");
		
		
	}
	
	//    **********************   end mouse area
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//       *******************   working ******************
	@Override
	public void run()
	{

		this.addMouseListeners();
		createDisplay();

	}

	protected void createDisplay()
	{
		
		this.addMouseListener(this);
		
		for (int i = 0; i < this.shapeList.length; i++)
		{
			this.addShape(this.shapeList[i]); 
		} 
	
	}
		
	
	
	protected void randomShape()
	{
		ShapeBaseClass[] sortshapeList = new ShapeBaseClass[this.shapeList.length];
		Random rgen = new Random();
		for (int i=0; i<shapeList.length; i++) 
		{
		    int randomPosition = rgen.nextInt(shapeList.length);
		    ShapeBaseClass temp = sortshapeList[i];
		    sortshapeList[i] = sortshapeList[randomPosition];
		    sortshapeList[randomPosition] = temp;
		}
		this.shapeList = sortshapeList;
	}
	
	protected ShapeBaseClass[] createObjects()
	{
		// create holder of shapeshapeListObjects
		ShapeBaseClass[] objectshapeList = new ShapeBaseClass[input.getTotalObject()];

	
		
		int highLimit = this.input.getRectangle();
		int lowLimit = 0;
		for (int i = lowLimit;  i < highLimit; i++)
		{
			objectshapeList[i] = createRectangle();
		}
		
		lowLimit = highLimit;
		highLimit = lowLimit+ input.getCircle();
		for (int i = lowLimit;  i < highLimit; i++)
		{
			objectshapeList[i] = createCircle();
		}
		
		lowLimit = highLimit;
		highLimit = lowLimit+ input.getTriangle();
		for (int i = lowLimit;  i < highLimit; i++)
		{
			objectshapeList[i] = createTriangle();
		}
		
		
		 return objectshapeList;
	}

	//   random generators 
	private int createRandom(int r)
	{
		Random rand = new Random(System.nanoTime());
		r = rand.nextInt(r);
		return r;
	}

	//   create shapes

	private Circle createCircle()
	{
		int h = createRandom(Constrants.CANVUSHEIGHT);
		int w = createRandom(Constrants.CANVUSWIDTH);
		Circle temp = new Circle((createRandom(Constrants.CANVUSHEIGHT)-h),
				(createRandom(Constrants.CANVUSWIDTH)-w), 
				h,
				createRandom(Constrants.COLORRED), 
				createRandom(Constrants.COLORGREEN), 
				createRandom(Constrants.COLORBLUE));
		return temp;
		
	}
	private Triangle createTriangle()
	{
		int h = createRandom(Constrants.CANVUSHEIGHT);
		int w = createRandom(Constrants.CANVUSWIDTH);
		GPolygon triangle = new GPolygon();
		triangle.addVertex(0, 0);
		triangle.addVertex(0, w);
		triangle.addVertex(w, h);
		int c1 = createRandom(Constrants.COLORRED);
		int c2 = createRandom(Constrants.COLORGREEN);
		int c3 = createRandom(Constrants.COLORBLUE);
		Color c = new Color(c1,c2,c3);
		triangle.setFillColor(c);
		Triangle temp = new Triangle(createRandom(Constrants.CANVUSHEIGHT), createRandom(Constrants.CANVUSWIDTH), h, w, c1, c2, c3, createRandom(1));
		return temp;
	}
	private Rectangle createRectangle()
	{
		int h = createRandom(Constrants.CANVUSHEIGHT);
		int w = createRandom(Constrants.CANVUSWIDTH);
		Rectangle temp = new Rectangle((createRandom(Constrants.CANVUSHEIGHT)-h),
				(createRandom(Constrants.CANVUSWIDTH)-w), 
				h,w,createRandom(Constrants.COLORRED), 
				createRandom(Constrants.COLORGREEN), 
				createRandom(Constrants.COLORBLUE));
		return temp;
	}
	

	protected void addShape(ShapeBaseClass shape)  // Add Shape Type  - Rectangle -Circle -Triangle
	{
		
		if (shape.getType() == "Rectangle")
		{
			GRect rect = new GRect(shape.getX(), shape.getY(), shape.getHeight(), shape.getWidth());
			rect.setColor(shape.getColor());
			rect.setFilled(true);
			rect.addMouseListener(this);
			this.add(rect);
		}
		else if (shape.getType() == "Circle")
		{
			GOval Cir = new GOval(shape.getX(), shape.getY(), shape.getHeight(), shape.getWidth());
			Cir.setColor(shape.getColor());
			Cir.setFilled(true);
			Cir.addMouseListener(this);
			this.add(Cir);	
		}
		else if (shape.getType() == "Triangle")
		{
			GPolygon triangle = new GPolygon();
			triangle.addVertex(0, 0);
			triangle.addVertex(0, shape.getWidth());
			triangle.addVertex(shape.getWidth(), shape.getHeight());
			triangle.setFillColor(shape.getColor());
			triangle.setFilled(true);
			this.add(triangle);
		}
		else
		{}
	}

	
	private void hold()
	{
		createRamdomNewShape();
		//createObjects();
		this.repaint();
		update = true;
		createDisplay();
		
		createRamdomNewShape();
		
	}
	
	private void printObjects()
	{
		for (int i = 0; i<this.input.getTotalObject();i++)
		{
			System.out.println("Obj type " + this.shapeList[i].typeObject + " X "+ this.shapeList[i].getX() + " Y " + this.shapeList[i].getY() + " H " + this.shapeList[i].getHeight() + " W " + this.shapeList[i].getWidth());
			System.out.println(" Color " + this.shapeList[i].getColor());
			System.out.println(" ");
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
}
